<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:param name="current-datetime"/>

<xsl:template match="/">
<osm-notes>
    <xsl:for-each select="osmChange/create/note">
        <note>
            <xsl:attribute name="id">
                <xsl:number format="-1" value="position()"/>
            </xsl:attribute>
            <xsl:attribute name="created_at">
                <xsl:value-of select="$current-datetime" />
            </xsl:attribute>
            <xsl:attribute name="lat">
                <xsl:value-of select="@lat" />
            </xsl:attribute>
            <xsl:attribute name="lon">
                <xsl:value-of select="@lon" />
            </xsl:attribute>
            <comment action="opened">
                <xsl:attribute name="timestamp">
                    <xsl:value-of select="$current-datetime"/>
                </xsl:attribute>
                <xsl:value-of select="comment/@text"/>
            </comment>
        </note>
    </xsl:for-each>
</osm-notes>
</xsl:template>

</xsl:stylesheet>
