#!/bin/sh

set -x -e

export OSM_DB_HOST=0.0.0.0
export OSM_DB_NAME=osm
export OSM_DB_PASSWORD=3820f01f4bc4
export OSM_DB_USER=rye

DB_HOSTS="localhost perigee.local"

pushd work

rm -v *.gpx *.osn* *.osc* *.json *-*.geojson || true

function do_work() {
    export OSM_DB_HOST=$1
    NEIGHBORHOOD=$2
    if [ -n "$NEIGHBORHOOD" ]; then
        echo "Sending $NEIGHBORHOOD processing to $OSM_DB_HOST"
        python3 ../boston-sam-generate-osm.py "$NEIGHBORHOOD"
    fi
}

(
    STOP=0
    while [[ $STOP == 0 ]] ; do
        for C in {1..3}; do
            for H in $DB_HOSTS; do
                if ! read X; then
                    STOP=1
                    break
                fi
                do_work $H "$X" &
            done
            if [[ $STOP == "1" ]]; then
                break
            fi
        done
        wait
    done
) < ../neighborhoods.txt

wait

# For CartoDB Visualization:
../node_modules/.bin/geojson-merge *-unique.geojson > boston_unique_buildings.geojson
../node_modules/.bin/geojson-merge *-fixmes.geojson > boston_fixmes.geojson
rm -v *-*.geojson

gzip -k -f -9 *.osc *.osn
gzip -f -9 boston_*.geojson

popd
