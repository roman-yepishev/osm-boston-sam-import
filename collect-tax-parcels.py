#!/usr/bin/python3

import fiona
import pyproj
import sys

import MySQLdb

from shapely.geometry import Polygon, LineString, MultiPoint, MultiPolygon
from utils import get_connection

SCHEMA = [
"""
DROP TABLE IF EXISTS `tax_parcels`;
""",
"""
CREATE TABLE `tax_parcels` (
    `id` BIGINT(64) PRIMARY KEY NOT NULL,
    `parcel` VARCHAR(64) NOT NULL,
    `pid_long` VARCHAR(64) NOT NULL,
    `poly` GEOMETRY NOT NULL,
    `centroid` POINT NOT NULL,
    SPATIAL KEY(`poly`),
    SPATIAL KEY(`centroid`)
) ENGINE=Aria
"""
]

INSERT_STMT = """INSERT INTO `tax_parcels` (
    `id`, `parcel`, `pid_long`, `poly`, `centroid`
) VALUES (
    %s, %s, %s, ST_GeomFromText(%s), PointFromText(%s)
)
"""

def assemble_polygon(shape, coordinates):
    if shape == 'MultiPolygon':
        poly = []
        for item in coordinates:
            poly.append(assemble_polygon('Polygon', item))

        poly = MultiPolygon(poly)
    elif shape == 'Polygon':
        x_list = [ p[0] for p in coordinates ]
        y_list = [ p[1] for p in coordinates ]

        lons, lats = conv(x_list, y_list, errcheck=True, inverse=True)

        poly = Polygon(LineString(MultiPoint(list(zip(lons, lats)))))
    else:
        raise ValueError('Unknown shape: {}'.format(shape))

    return poly

if __name__ == "__main__":

    sf = fiona.open(sys.argv[1])

    conn = get_connection()
    cursor = conn.cursor()

    conv = pyproj.Proj(sf.crs, preserve_units=True)

    for item in SCHEMA:
        cursor.execute(item)

    stmt_buffer = []

    for feature_id, feature in sf.items():
        props = feature['properties']
        parcel = props['PARCEL']
        pid_long = props['PID_LONG']

        geometry = feature['geometry']

        poly = assemble_polygon(geometry['type'], geometry['coordinates'][0])

        stmt_buffer.append((feature_id, parcel, pid_long, poly, poly.centroid))

        if len(stmt_buffer) > 500:
            cursor.executemany(INSERT_STMT, stmt_buffer)
            stmt_buffer = []

    if len(stmt_buffer) > 0:
        cursor.executemany(INSERT_STMT, stmt_buffer)
