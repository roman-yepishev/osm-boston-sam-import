"""Utility classes for SAM import"""

from datetime import datetime
import xml.etree.ElementTree as ET

import os

import MySQLdb
import MySQLdb.cursors

BANNER = 'Boston SAM Import (https://wiki.openstreetmap.org/wiki/Import/Boston_Street_Address_Management_(SAM)_Import)'

DEV_VERSION_INCREMENT = 10

def get_connection():
    params = {
        'host': os.environ.get('OSM_DB_HOST', 'localhost'),
        'user': os.environ.get('OSM_DB_USER', 'osm'),
        'db': os.environ.get('OSM_DB_NAME', 'osm'),
        'charset': 'utf8',
        'use_unicode': True,
        'cursorclass': MySQLdb.cursors.DictCursor
    }

    if os.environ.get('OSM_DB_PASSWORD') is not None:
        params['passwd'] = os.environ.get('OSM_DB_PASSWORD')

    return MySQLdb.connect(**params)


class OSNSerializer(object):
    """.osn XML serializer"""

    def serialize(self, notes):
        now = datetime.now().isoformat('T')
        osm_notes = ET.Element('osm-notes')
        counter = 0
        for item in notes:
            counter -= 1
            note = ET.SubElement(osm_notes, 'note', {
                'id': str(counter),
                'lat': str(item['location'].y),
                'lon': str(item['location'].x),
                'created_at': now
            })
            comment = ET.SubElement(note, 'comment', {
                'action': 'opened',
                'timestamp': now
            })
            comment.text = item['text']

        return ET.tostring(osm_notes, encoding='UTF-8')

class OSCSerializer(object):
    """.osc XML serializer

    Please note that it is hacked together specifically for
    the purpose of the import and you should never use it
    in your program.
    """

    def _serialize_nodes(self, root, nodes):
        create = ET.SubElement(root, 'create')

        for node_id, node in enumerate(nodes):
            node_id = -1 - node_id

            node_xml = ET.SubElement(create, 'node', {
                'version': '1',
                'id': str(node_id),
                'lat': str(node['location'].y),
                'lon': str(node['location'].x),
            })

            for k, v in node['tags'].items():
                node_tag = ET.SubElement(node_xml, 'tag', {
                    'k': k,
                    'v': v
                })

    def _serialize_ways(self, root, ways):
        modify = ET.SubElement(root, 'modify')

        for way_id in sorted(ways.keys()):
            data = ways[way_id]

            way_xml = ET.SubElement(modify, 'way', {
                'id': str(way_id),
                # Don't allow server to merge this right in
                'version': str(data['version'] + DEV_VERSION_INCREMENT)
            })

            for node_id in data['nd']:
                nd = ET.SubElement(way_xml, 'nd', {
                    'ref': str(node_id)
                })

            for key, value in data['tags'].items():
                tag = ET.SubElement(way_xml, 'tag', {
                    'k': key,
                    'v': value
                })

    def serialize(self, ways, nodes=None):
        osm_change = ET.Element('osmChange', {
            'generator': BANNER,
            'version': '0.6'
        })

        if ways is not None:
            self._serialize_ways(osm_change, ways)

        if nodes is not None:
            self._serialize_nodes(osm_change, nodes)

        return ET.tostring(osm_change, encoding='UTF-8')

class GPXSerializer(object):
    """.gpx XML serializer for notes"""
    def serialize(self, notes):
        gpx = ET.Element('gpx', {
            'version': '1.1',
            'creator': BANNER,
            'xmlns': 'http://www.topografix.com/GPX/1/1'
        })

        now = datetime.utcnow().isoformat('T')

        for item in notes:
            wpt = ET.SubElement(gpx, 'wpt', {
                'lat': str(item['location'].y),
                'lon': str(item['location'].x)
            })

            time = ET.SubElement(wpt, 'time')
            time.text = now
            name = ET.SubElement(wpt, 'name')
            name.text = item['text']

            if 'comment' in item:
                desc = ET.SubElement(wpt, 'desc')
                desc.text = item['comment']

        return ET.tostring(gpx, encoding='UTF-8')

if __name__ == "__main__":
    from shapely.geometry import Point
    notes = [{'location': Point(12.345678,-54.3210), 'text': 'Cool place'}]

    osn = OSNSerializer()
    print(osn.serialize(notes))

    gpx = GPXSerializer()
    print(gpx.serialize(notes))

