#!/usr/bin/python3

import os
import json

from time import strftime

from mediafire.client import MediaFireClient

a = MediaFireClient()

data = {}

neighborhoods = sorted([
    n.strip() for n in open('neighborhoods.txt').read().split('\n') if len(n) > 0
])

extras = {}

for x in a.get_folder_contents_iter('j7lb1tuhwd17i'):
    filename = x['filename']

    if filename.endswith('.pbf') or filename.endswith('.obf'):
        extras[filename] = {
                'url': x['links']['normal_download'],
                'modified': x.get('modified', x.get('created', '')),
                'size': x['size']
        }

    neighborhood = filename.split('-')[0].split('.')[0]
    neighborhood = neighborhood.replace('_', '/')

    if neighborhood not in neighborhoods:
        # skip, that's some other file
        continue

    if neighborhood not in data:
        data[neighborhood] = {}

    if filename.endswith('-unique.osc.gz'):
        data[neighborhood]['u'] = x['links']['normal_download']
    
    if filename.endswith('-fixmes.osc.gz'):
        data[neighborhood]['f'] = x['links']['normal_download']

    if filename.endswith('-addresses.osc.gz'):
        data[neighborhood]['a'] = x['links']['normal_download']

    if filename.endswith('.poly'):
        data[neighborhood]['p'] = x['links']['normal_download']

    if filename.endswith('.osn.gz'):
        data[neighborhood]['n'] = x['links']['normal_download']

    if filename.endswith('.gpx'):
        data[neighborhood]['g'] = x['links']['normal_download']

result = [
    '{| class="wikitable"',
    '!Name / Poly',
    '!Unique buildings',
    '!Address nodes',
    '!Fixme buildings',
    '!Missing/fixme building markers'
]


for neighborhood in neighborhoods:
    filename = neighborhood.replace('/', '_')

    result.append('|-')
    stats_file = "work/{}-stats.json".format(filename)
    if os.path.exists(stats_file):
        with open(stats_file) as fh:
            stats = json.loads(fh.read())

        result.extend([
            "|[{} {}]".format(data[neighborhood]['p'], neighborhood),
            "|[{} {}]".format(data[neighborhood]['u'], stats['u']),
            "|[{} {}]".format(data[neighborhood]['a'], stats['a']),
            "|[{} {}]".format(data[neighborhood]['f'], stats['f']),
            "|{} [{} .gpx], [{} .osn.gz]".format(
                stats['n'],
                data[neighborhood]['g'],
                data[neighborhood]['n']
            )
        ])
    else:
        result.extend([
            '|' + neighborhood,
            '|colspan="4"|Generation failed'
        ])

result.append('|}')
result.append('')
result.append('Last updated: {}'.format(strftime('%Y-%m-%d %H:%M:%S%z')))
result.append('')
result.append('==== Previews ====')
result.append('')
result.extend([
    '{| class="wikitable"',
    '!File',
    '!Description',
    '!Size',
    '!Last updated',
])


for filename in sorted(extras.keys()):
    data = extras[filename]
    result.append('|-')
    result.append('|[{} {}]'.format(data['url'], filename))
    if filename.endswith('.pbf'):
        result.append('|OSM PBF export + unique Boston SAM numbers')
    elif filename.endswith('.obf'):
        result.append('|OsmAnd map + unique Boston SAM numbers')
    result.append('|{}'.format(data['size']))
    result.append('|{}'.format(data['modified']))

result.append('|}')

print('\n'.join(result))
