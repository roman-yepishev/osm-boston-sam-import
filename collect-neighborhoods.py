#!/usr/bin/python3

import os

import shapely
import shapely.wkt

from shapely.geometry import (LineString, MultiPoint, mapping)
from shapely.ops import polygonize

import fiona

from polygon2poly import PolySerializer

from utils import get_connection

SCHEMA = [
    """
    DROP TABLE IF EXISTS `neighborhoods`
    """,
    """
    CREATE TABLE `neighborhoods` (
        `id` SMALLINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        `name` VARCHAR(64),
        `poly` POLYGON NOT NULL
    ) ENGINE=Aria
    """
]

if __name__ == "__main__":
    names = []

    with open('neighborhoods.txt', 'r') as fh:
        names = [ line.strip() for line in fh if len(line) > 0 ]

    conn = get_connection()

    cursor = conn.cursor()

    for item in SCHEMA:
        cursor.execute(item)

    res = cursor.execute("""
    SELECT
        `relation_tags1`.`v` AS `neighborhood_name`,
        `way_nodes`.`id` AS `way_id`,
        `way_nodes`.`sequence_id` AS `sequence_id`,
        AsWKT(`node_positions`.`pos`) AS `pos`
    FROM
        `relation_members`
    INNER JOIN
        `relation_tags` AS `relation_tags1` ON `relation_tags1`.`id` = `relation_members`.`id`
    INNER JOIN
        `relation_tags` AS `relation_tags2` ON `relation_tags2`.`id` = `relation_members`.`id`
    RIGHT JOIN
        `way_nodes` ON `way_nodes`.`id` = `relation_members`.`member_id`
    LEFT JOIN
        `nodes` ON `nodes`.`id` = `way_nodes`.`node_id`
    LEFT JOIN
        `node_positions` ON `node_positions`.`id` = `nodes`.`id`
    WHERE
        `relation_tags1`.k = 'name'
        AND `relation_tags2`.k = 'boundary'
        AND `relation_tags2`.v = 'neighborhood'
    ORDER BY `neighborhood_name`, `way_id`, `sequence_id`
    """)

    neighborhoods = {}

    lines = {}
    for row in cursor:
        neighborhood_name = row['neighborhood_name']

        if neighborhood_name not in names:
            print("Stray neighborhood: {}, skipping".format(neighborhood_name))
            continue

        if neighborhood_name not in neighborhoods:
            lines = {}
            neighborhoods[neighborhood_name] = lines

        if row['way_id'] not in lines:
            lines[row['way_id']] = []

        lines[row['way_id']].append(
            shapely.wkt.loads(row['pos'])
        )

    result = []

    for name, lines in neighborhoods.items():
        for way_id in lines.keys():
            lines[way_id] = LineString(MultiPoint(lines[way_id]))

        print("Found neighborhood {}: {} lines".format(
            name, len(lines.keys())))

        neighborhood_poly = list(polygonize(lines.values()))[0]

        result.append([0, name, neighborhood_poly])


    cursor.executemany(
            """INSERT INTO `neighborhoods` VALUES(%s, %s, PolyFromText(%s))""",
            result)

    schema = {
        'geometry': 'Polygon',
        'properties': {'name': 'str:64'}
    }

    print("Writing .poly and .geojson files...")

    for item in result:
        path_basename = os.path.join('work', item[1].replace('/', '_'))

        s = PolySerializer()
        with open('{}.poly'.format(path_basename), 'w') as output:
            output.write(s.serialize(item[2], item[1]))

        geojson_path = '{}.geojson'.format(path_basename)

        if os.path.exists(geojson_path):
            os.unlink(geojson_path)

        with fiona.open(geojson_path, 'w', 'GeoJSON', schema) as output:
            output.write({
                'geometry': mapping(item[2]),
                'properties': {
                    'name': item[1]
                }
            })

    print("Done")
