#!/usr/bin/python3

import sys
import xml.sax

from utils import get_connection

SCHEMA = [
'DROP TABLE IF EXISTS relation_tags',
'DROP TABLE IF EXISTS relation_members',
'DROP TABLE IF EXISTS relations',
'DROP TABLE IF EXISTS way_tags',
'DROP TABLE IF EXISTS way_nodes',
'DROP TABLE IF EXISTS ways',
'DROP TABLE IF EXISTS node_tags',
'DROP TABLE IF EXISTS node_positions',
'DROP TABLE IF EXISTS nodes',

"""
CREATE TABLE nodes (
    `id` BIGINT(64) NOT NULL PRIMARY KEY,
    `version` INTEGER,
    `visible` ENUM('true', 'false'),
    `changeset` BIGINT(64) NULL,
    `timestamp` DATETIME NULL,
    `uid` BIGINT(64) NULL,
    `user` VARCHAR(255) NULL,
    `lat` DOUBLE NOT NULL,
    `lon` DOUBLE NOT NULL
) DEFAULT CHARSET=utf8
""",

"""
CREATE TABLE node_tags (
    `id` BIGINT(64) NOT NULL,
    `k` VARCHAR(255) NOT NULL DEFAULT '',
    `v` VARCHAR(255) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`, `k`),
    CONSTRAINT `fk_node_tags_node`
        FOREIGN KEY (`id`) REFERENCES nodes(`id`)
) DEFAULT CHARSET=utf8
""",

"""
CREATE TABLE IF NOT EXISTS ways (
    `id` BIGINT(64) NOT NULL PRIMARY KEY,
    `timestamp` DATETIME NULL,
    `uid` BIGINT(64) NULL,
    `user` VARCHAR(255) NULL,
    `version` INTEGER,
    `visible` ENUM('true', 'false'),
    `changeset` BIGINT(64) NULL
) DEFAULT CHARSET=utf8
""",

"""
CREATE TABLE IF NOT EXISTS way_tags (
    `id` BIGINT(64) NOT NULL,
    `k` VARCHAR(255) NOT NULL DEFAULT '',
    `v` VARCHAR(255) NOT NULL DEFAULT '',
    CONSTRAINT `fk_way_tags_way`
        FOREIGN KEY (`id`) REFERENCES ways(`id`)
) DEFAULT CHARSET=utf8
""",
"""
CREATE TABLE way_nodes (
    `id` BIGINT(64),
    `node_id` BIGINT(64) NOT NULL,
    `sequence_id` BIGINT(11) NOT NULL,
    CONSTRAINT `fk_way_nodes_way`
        FOREIGN KEY (`id`) REFERENCES ways(`id`)
) DEFAULT CHARSET=utf8
""",

"""
CREATE TABLE IF NOT EXISTS relations (
    `id` BIGINT(64) NOT NULL,
    `changeset` BIGINT(64) NULL,
    `timestamp` DATETIME NULL,
    `version` INTEGER,
    `uid` BIGINT(64) NULL,
    `user` VARCHAR(255) NULL,
    `visible` ENUM('true', 'false'),
    PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8
""",

"""
CREATE TABLE relation_members (
    `id` BIGINT(64) NOT NULL,
    `member_type` ENUM('node', 'way', 'relation') NOT NULL,
    `member_id` BIGINT(64) NOT NULL,
    `member_role` VARCHAR(255) NOT NULL DEFAULT '',
    `sequence_id` BIGINT(11) NOT NULL DEFAULT 0,
    CONSTRAINT `fk_relation_members_relation`
        FOREIGN KEY (`id`) REFERENCES relations(`id`)
) DEFAULT CHARSET=utf8
""",

"""
CREATE TABLE IF NOT EXISTS relation_tags (
    `id` BIGINT(64) NOT NULL,
    `k` VARCHAR(255) NOT NULL DEFAULT '',
    `v` VARCHAR(255) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`, `k`),
    CONSTRAINT `fk_relation_tags_relation`
        FOREIGN KEY (`id`) REFERENCES relations(`id`)
) DEFAULT CHARSET=utf8
""",

"""
CREATE TABLE `node_positions` (
    `id` bigint(64) NOT NULL,
    `pos` geometry NOT NULL,
    KEY `id` (`id`),
    SPATIAL KEY `pos` (`pos`)
) ENGINE=Aria
"""
]

class OSMContentHandler(xml.sax.ContentHandler):
    def __init__(self, db):
        self._cursor = db.cursor()
        self._db = db
        self._type = None
        self._id = None
        self._counter = 0
        self._last_type = None
        self._multiinsert = {}

    def startElement(self, name, attrs):
        if name == 'way':
            self._type = name
            self._id = attrs['id']
            self._add_way(self._id, attrs)

        if name == 'node':
            self._type = name
            self._id = attrs['id']
            self._add_node(self._id, attrs)
        if name == 'relation':
            self._type = name
            self._id = attrs['id']
            self._add_relation(self._id, attrs)

        if name in ['way', 'node', 'relation']:
            if self._last_type != name:
                self._multiinsert_flush(True)
                self._commit()
                self._last_type = name

        if name == 'nd':
            self._add_way_node(self._id, attrs)

        if name == 'member':
            self._add_relation_member(self._id, attrs)

        if name == 'tag':
            if self._id is not None:
                self._add_tag(self._type, self._id, attrs['k'], attrs['v'])
            else:
                raise ValueError("Stray tag without a container")

    def endElement(self, name):
        if self._type == name:
            self._id = None
            self._type = None
            self._counter = 0

        if name == 'osm':
            self._multiinsert_flush(True)

    def _multiinsert_add(self, priority, stmt, args):
        if priority not in self._multiinsert:
            self._multiinsert[priority] = {}

        if stmt not in self._multiinsert[priority]:
            self._multiinsert[priority][stmt] = []

        # self._cursor.execute(stmt, args)
        self._multiinsert[priority][stmt].append(args)
        self._multiinsert_flush(False)


    @staticmethod
    def _chunks(iterable, n):
        return [iterable[x:x+n] for x in range(0, len(iterable), n)]

    def _multiinsert_flush(self, force=False):

        count = 0

        for prio in self._multiinsert.keys():
            for stmt_data in self._multiinsert[prio].values():
                count += len(stmt_data)

        if not force and count < 50000:
            return

        print("Flushing {} records...".format(count))

        for priority in sorted(self._multiinsert.keys()):
            prio_cache = self._multiinsert[priority]
            for stmt, items in prio_cache.items():
                for chunk in  self._chunks(items, 2000):
                    self._cursor.executemany(stmt, chunk)
                prio_cache[stmt] = []

    def _add_node(self, id_, attrs):
        stmt = '''
            INSERT INTO nodes (
                `id`,
                `version`,
                `visible`,
                `changeset`,
                `timestamp`,
                `uid`,
                `user`,
                `lat`,
                `lon`
            )
            VALUES(
                %(id)s,
                %(version)s,
                %(visible)s,
                %(changeset)s,
                %(timestamp)s,
                %(uid)s,
                %(user)s,
                %(lat)s,
                %(lon)s
            )
        '''
        self._multiinsert_add(0, stmt, {
            'id': id_,
            'version': attrs.get('version', 0),
            'visible': attrs.get('visible', 'true'),
            'changeset': attrs.get('changeset'),
            'timestamp': attrs.get('timestamp'),
            'uid': attrs.get('uid', 0),
            'user': attrs.get('user', ''),
            'lat': attrs['lat'],
            'lon': attrs['lon']
        })

        self._multiinsert_add(1, """
            INSERT INTO node_positions (
                `id`, `pos`
            ) VALUES (
                %s,
                PointFromText(%s)
            )
        """, (id_, 'POINT({} {})'.format(attrs['lon'], attrs['lat'])))

    def _add_way_node(self, id_, attrs):
        stmt = '''
            INSERT INTO way_nodes (
                `id`,
                `node_id`,
                `sequence_id`
            ) VALUES (
                %(id)s,
                %(node_id)s,
                %(sequence_id)s
            )
        '''

        self._multiinsert_add(3, stmt, {
            'id': id_,
            'node_id': attrs['ref'],
            'sequence_id': self._counter
        })

        self._counter += 1

    def _add_way(self, id_, attrs):
        stmt = """
            INSERT INTO ways(
                `id`,
                `timestamp`,
                `uid`,
                `user`,
                `version`,
                `visible`,
                `changeset`
            ) VALUES(
                %(id)s,
                %(timestamp)s,
                %(uid)s,
                %(user)s,
                %(version)s,
                %(visible)s,
                %(changeset)s
            )
        """
        self._multiinsert_add(0, stmt, {
            'id': id_,
            'timestamp': attrs.get('timestamp', None),
            'uid': attrs.get('uid', 0),
            'user': attrs.get('user', ''),
            'version': attrs.get('version', 0),
            'visible': attrs.get('visible', 'true'),
            'changeset': attrs.get('changeset', 0)
        })

    def _add_relation(self, id_, attrs):
        stmt = """
            INSERT INTO relations(
                `id`,
                `changeset`,
                `timestamp`,
                `version`,
                `uid`,
                `user`,
                `visible`
            ) VALUES (
                %(id)s,
                %(changeset)s,
                %(timestamp)s,
                %(version)s,
                %(uid)s,
                %(user)s,
                %(visible)s
            )
        """
        self._multiinsert_add(0, stmt, {
            'id': id_,
            'changeset': attrs.get('changeset', 0),
            'timestamp': attrs.get('timestamp', None),
            'version': attrs.get('version', 0),
            'uid': attrs.get('uid', 0),
            'user': attrs.get('user', ''),
            'visible': attrs.get('visible', 'true')
        })

    def _add_relation_member(self, id_, attrs):
        stmt = '''
            INSERT INTO relation_members (
                `id`,
                `member_type`,
                `member_id`,
                `member_role`,
                `sequence_id`
            )
            VALUES(
                %(id)s,
                %(member_type)s,
                %(member_id)s,
                %(member_role)s,
                %(sequence_id)s
            )
        '''
        self._multiinsert_add(1, stmt, {
            'id': id_,
            'member_type': attrs['type'],
            'member_id': attrs['ref'],
            'member_role': attrs['role'],
            'sequence_id': self._counter
        })
        self._counter += 1

    def _add_tag(self, type_, id_, key, value):
        stmt = 'INSERT INTO ' + type_ + '_tags VALUES(%s, %s, %s)'
        self._multiinsert_add(2, stmt, (id_, key, value))

    def _commit(self):
        self._db.commit()

if __name__ == "__main__":

    osm_path = sys.argv[1]

    db = get_connection()

    cursor = db.cursor()

    for table_stmt in SCHEMA:
        try:
            cursor.execute(table_stmt)
        except Exception as e:
            print("Exception: {}\n\tfor {}".format(e, table_stmt))
            raise e

    handler = OSMContentHandler(db)
    parser = xml.sax.make_parser()
    parser.setContentHandler(handler)

    with open(osm_path, 'r', encoding='UTF-8') as fh:
        parser.parse(fh)

    db.commit()
    db.close()
