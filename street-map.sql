-- This is a set of persistent tables
--
-- Run this after importing SAM database and loading the .osm file
--
-- We map the SAM addresses to OSM streets
-- Then we map the remaining things via regexp like (not many matches)
-- Then we add additional mappings as SQL statements

SET NAMES 'utf8';

DROP TABLE IF EXISTS `osm_sam_street_mappings`;
DROP TABLE IF EXISTS `osm_streets`;
DROP TABLE IF EXISTS `sam_streets`;


CREATE TABLE `osm_streets` (
    `id` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(256),
    `short_name` VARCHAR(256),
    `alt_name` VARCHAR(256),
    KEY(`name`),
    KEY(`short_name`),
    KEY(`alt_name`)
) CHARSET=utf8;

CREATE TABLE `sam_streets` (
    `id` INTEGER NOT NULL PRIMARY KEY,
    `name` VARCHAR(256),
    KEY(`name`)
) CHARSET=utf8;

CREATE TABLE `osm_sam_street_mappings` (
    `sam_street_id` INTEGER NOT NULL PRIMARY KEY,
    `osm_street_id` INTEGER NULL,
    `fuzzy` BOOLEAN,
    CONSTRAINT FOREIGN KEY(`sam_street_id`) REFERENCES `sam_streets`(`id`),
    CONSTRAINT FOREIGN KEY(`osm_street_id`) REFERENCES `osm_streets`(`id`)
);


INSERT INTO `sam_streets`
    SELECT DISTINCT
        `STREET_ID`,
        CONCAT(
            IFNULL(CONCAT(`STREET_PREFIX`, ' '), ''),
            TRIM(`STREET_BODY`),
            IFNULL(CONCAT(' ', `STREET_FULL_SUFFIX`), '')
        )
    FROM
        `SAM_Addresses`;

SHOW WARNINGS;

INSERT INTO `osm_streets`
    SELECT DISTINCT
        0,
        `name_tag`.`v`,
        `short_name_tag`.`v`,
        `alt_name_tag`.`v`
    FROM
        `way_tags` AS `highway_tag`
    INNER JOIN
        `way_tags` AS `name_tag`
        ON `name_tag`.`id` = `highway_tag`.`id` AND `name_tag`.`k` = 'name'
    LEFT JOIN
        `way_tags` AS `short_name_tag`
        ON `short_name_tag`.`id` = `highway_tag`.`id` AND `short_name_tag`.`k` = 'short_name'
    LEFT JOIN
        `way_tags` AS `alt_name_tag`
        ON `alt_name_tag`.`id` = `highway_tag`.`id` AND `alt_name_tag`.`k` = 'alt_name'
    WHERE `highway_tag`.`k` = 'highway';

SHOW WARNINGS;

-- 1-1 mapping, but only the first match wins

INSERT IGNORE INTO `osm_sam_street_mappings` 
	SELECT
        `sam_streets`.`id`,
        `osm_streets`.`id`,
        FALSE
    FROM
		`sam_streets`
	LEFT JOIN
		`osm_streets`
	ON
		(`sam_streets`.`name` = `osm_streets`.`name` OR
         `sam_streets`.`name` = `osm_streets`.`short_name` OR
         `sam_streets`.`name` = `osm_streets`.`alt_name`);

REPLACE INTO `osm_sam_street_mappings` 
	SELECT
		`sam_streets`.`id`,
		`osm_streets`.`id`,
		TRUE
	FROM
		`osm_sam_street_mappings`
	INNER JOIN `sam_streets`
	    ON `sam_streets`.`id` = `osm_sam_street_mappings`.`sam_street_id`
	INNER JOIN `osm_streets`
        ON (
            `osm_streets`.`name` LIKE REGEXP_REPLACE(`sam_streets`.`name`, '[^[:alnum:]]+', '%') OR
            `osm_streets`.`short_name` LIKE REGEXP_REPLACE(`sam_streets`.`name`, '[^[:alnum:]]+', '%') OR
            `osm_streets`.`alt_name` LIKE REGEXP_REPLACE(`sam_streets`.`name`, '[^[:alnum:]]+', '%')
        )
	WHERE
        `osm_sam_street_mappings`.`osm_street_id` IS NULL;

---
--- Procedure for adding mapping
---
DROP PROCEDURE IF EXISTS `sp_street_map_add`;

DELIMITER //

CREATE PROCEDURE `sp_street_map_add` (
    IN `osm_street` VARCHAR(256),
    IN `sam_street` VARCHAR(256)
)
BEGIN
    SELECT CONCAT('Mapping OSM:', `osm_street`, ' to SAM:', `sam_street`) AS `message`;
    UPDATE `osm_sam_street_mappings`
    SET `fuzzy` = FALSE,
    `osm_street_id` = (
        SELECT `id` FROM `osm_streets` WHERE `name` = osm_street
    )
    WHERE
        `sam_street_id` = (
            SELECT `id` FROM `sam_streets` WHERE `name` = sam_street
        );
END//

DELIMITER ;

--
-- insert additional mappings here
--

-- OSM street, SAM street
CALL sp_street_map_add('Roseway', 'Roseway Street');
CALL sp_street_map_add('Surryhill Lane', 'Surreyhill Lane');
CALL sp_street_map_add('South Huntington Avenue', 'S Huntington Avenue');
CALL sp_street_map_add('John A Andrew Street', 'John A. Andrew Street');
CALL sp_street_map_add('North Anderson Street','N Anderson Street');
CALL sp_street_map_add('South Bay Avenue','S Bay Avenue');
CALL sp_street_map_add('North Beacon Street','N Beacon Street');
CALL sp_street_map_add('West Bellflower Street','W Bellflower Street');
CALL sp_street_map_add('North Bennet Place','N Bennet Place');
CALL sp_street_map_add('North Bennet Street','N Bennet Street');
CALL sp_street_map_add('North Bennet Court','N Bennet Court');
CALL sp_street_map_add('East Berkeley Street','E Berkeley Street');
CALL sp_street_map_add('West Boundary Road','W Boundary Road');
CALL sp_street_map_add('South Bremen Street','S Bremen Street');
CALL sp_street_map_add('East Broadway','E Broadway');
CALL sp_street_map_add('West Broadway','W Broadway');
CALL sp_street_map_add('East Brookline Street','E Brookline Street');
CALL sp_street_map_add('West Brookline Street','W Brookline Street');
CALL sp_street_map_add('East Canton Street','E Canton Street');
CALL sp_street_map_add('West Canton Street','W Canton Street');
CALL sp_street_map_add('South Cedar Place','S Cedar Place');
CALL sp_street_map_add('West Cedar Street','W Cedar Street');
CALL sp_street_map_add('North Charlame Court','N Charlame Court');
CALL sp_street_map_add('North Charlame Terrace','N Charlame Terrace');
CALL sp_street_map_add('South Charlame Court','S Charlame Court');
CALL sp_street_map_add('South Charlame Terrace','S Charlame Terrace');
CALL sp_street_map_add('East Concord Street','E Concord Street');
CALL sp_street_map_add('West Concord Street','W Concord Street');
CALL sp_street_map_add('East Cottage Street','E Cottage Street');
CALL sp_street_map_add('West Cottage Street','W Cottage Street');
CALL sp_street_map_add('North Crescent Circuit','N Crescent Circuit');
CALL sp_street_map_add('South Crescent Circuit','S Crescent Circuit');
CALL sp_street_map_add('Crowley Rogers Way','Crowley-Rogers Way');
CALL sp_street_map_add('David G. Mugar Way','David G Mugar Way');
CALL sp_street_map_add('East Dedham Street','E Dedham Street');
CALL sp_street_map_add('West Dedham Street','W Dedham Street');
CALL sp_street_map_add('Drydock Avenue','Dry Dock Avenue');
CALL sp_street_map_add('East Eagle Street','E Eagle Street');
CALL sp_street_map_add('West Eagle Street','W Eagle Street');
CALL sp_street_map_add('East Eighth Street','E Eighth Street');
CALL sp_street_map_add('West Eighth Street','W Eighth Street');
CALL sp_street_map_add('South Fairview Street','S Fairview Street');
CALL sp_street_map_add('East Fifth Street','E Fifth Street');
CALL sp_street_map_add('West Fifth Street','W Fifth Street');
CALL sp_street_map_add('East First Street','E First Street');
CALL sp_street_map_add('West First Street','W First Street');
CALL sp_street_map_add('East Fourth Street','E Fourth Street');
CALL sp_street_map_add('West Fourth Street','W Fourth Street');
CALL sp_street_map_add('North Grove Street','N Grove Street');
CALL sp_street_map_add('Hallet Davis Street','Hallet-Davis Street');
CALL sp_street_map_add('North Hanover Court','N Hanover Court');
CALL sp_street_map_add('North Harvard Street','N Harvard Street');
CALL sp_street_map_add('West Hill Place','W Hill Place');
CALL sp_street_map_add('South Hobart Street','S Hobart Street');
CALL sp_street_map_add('West Howell Street','W Howell Street');
CALL sp_street_map_add('North Hudson Street','N Hudson Street');
CALL sp_street_map_add('East India Row','E India Row');
CALL sp_street_map_add('Joyce-Hayes Way','Joyce Hayes Way');
CALL sp_street_map_add('Legends Highway','Legends Way');
CALL sp_street_map_add('East Lenox Street','E Lenox Street');
CALL sp_street_map_add('North Margin Street','N Margin Street');
CALL sp_street_map_add('Maryanna Road','Mary Anna Road');
CALL sp_street_map_add('North Mead Street','N Mead Street');
CALL sp_street_map_add('North Mead Street Court','N Mead Street Court');
CALL sp_street_map_add('West Milton Street','W Milton Street');
CALL sp_street_map_add('North Munroe Terrace','N Munroe Terrace');
CALL sp_street_map_add('South Munroe Terrace','S Munroe Terrace');
CALL sp_street_map_add('East Newton Street','E Newton Street');
CALL sp_street_map_add('West Newton Street','W Newton Street');
CALL sp_street_map_add('East Ninth Street','E Ninth Street');
CALL sp_street_map_add('West Ninth Street','W Ninth Street');
CALL sp_street_map_add('Orton Marotta Way','Orton-Marotta Way');
-- CALL sp_street_map_add('Woodward Park Street','W Park Street');
-- ?
-- CALL sp_street_map_add('Rowen Court','Rowe Court');
CALL sp_street_map_add('South Russell Street','S Russell Street');
CALL sp_street_map_add('West Rutland Square','W Rutland Square');
CALL sp_street_map_add('West School Street','W School Street');
CALL sp_street_map_add('East Second Street','E Second Street');
CALL sp_street_map_add('West Second Street','W Second Street');
CALL sp_street_map_add('West Selden Street','W Selden Street');
CALL sp_street_map_add('East Seventh Street','E Seventh Street');
CALL sp_street_map_add('West Seventh Street','W Seventh Street');
CALL sp_street_map_add('East Sixth Street','E Sixth Street');
CALL sp_street_map_add('West Sixth Street','W Sixth Street');
CALL sp_street_map_add('Snowhill Street','Snow Hill Street');
CALL sp_street_map_add('West Sorrento Street','W Sorrento Street');
CALL sp_street_map_add('East Springfield Street','E Springfield Street');
CALL sp_street_map_add('West Springfield Street','W Springfield Street');
CALL sp_street_map_add('South Sydney Street','S Sydney Street');
CALL sp_street_map_add('East Third Street','E Third Street');
CALL sp_street_map_add('West Third Street','W Third Street');
CALL sp_street_map_add('West Third Street Place','W Third Street Place');
CALL sp_street_map_add('West Tremlett Street','W Tremlett Street');
-- CALL sp_street_map_add('Walnut Hill Road','Walnut Road');
CALL sp_street_map_add('West Walnut Park','W Walnut Park');
CALL sp_street_map_add('South Walter Street','S Walter Street');
CALL sp_street_map_add('Washington Street Place','Washington Place');
CALL sp_street_map_add('North Washington Street','N Washington Street');
CALL sp_street_map_add('South Waverly Street','S Waverly Street');
CALL sp_street_map_add('South Whitney Street','S Whitney Street');
CALL sp_street_map_add('William T. Morrissey Boulevard','William T Morrissey Boulevard');
CALL sp_street_map_add('Brannon Harris Way','Brannon-Harris Way');
-- CALL sp_street_map_add('Randlett Place','Rand Place');
-- CALL sp_street_map_add('Thornton Terrace','Thor Terrace');
-- CALL sp_street_map_add('Franklin Park Service Road','Franklin Park Road');
CALL sp_street_map_add('La Grange Place', 'Lagrange Place');
CALL sp_street_map_add('Reverend R. A. Burke Street', 'Rev Richard A Burke Street');
CALL sp_street_map_add('Doctor Michael Gavin Way', 'Dr. Michael Gavin Way');
CALL sp_street_map_add("James O'Neill Street", 'James ONeill Street');
CALL sp_street_map_add("Major Michael J O'Connor Way", 'Major Michael J Oconnor Way');
CALL sp_street_map_add("Monsignor John J. O'Donnell Square", 'Monsignor John J. ODonnell Square');
CALL sp_street_map_add('Reverend Robert M. Costello Place', 'Rev Robert M Costello Place');
CALL sp_street_map_add('Leo M. Birmingham Parkway', 'Leo M Birmingham Parkway');
CALL sp_street_map_add("Saint Luke's Road", 'Saint Lukes Road');
CALL sp_street_map_add("McClellan Highway", 'William F McClellan Highway');
CALL sp_street_map_add("Anthony J. Grieco Terrace", "Anthony J Grieco Terrace");
CALL sp_street_map_add("O'Meara Court", 'OMeara Court');
CALL sp_street_map_add("O'Brien Court", 'OBrien Court');
CALL sp_street_map_add('Veterans Of Foreign Wars Parkway', 'VFW Parkway');
CALL sp_street_map_add('Doctor Mary Moore Beatty Circle', 'Dr. Mary Moore Beatty Circle');

-- Storrow Drive is more official name for that road
CALL sp_street_map_add('Storrow Drive', 'James J Storrow Memorial Drive');

CALL sp_street_map_add("William Cardinal O'Connell Way", 'William Cardinal OConnell Way');

CALL sp_street_map_add("Copp's Hill Terrace", 'Copps Hill Terrace');
