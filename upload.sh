#!/bin/bash

TARGET=mf:j7lb1tuhwd17i/

function do_upload() {
    rv=1
    while [ $rv != 0 ]; do
        mediafire-cli file-upload "$@" $TARGET
        rv=$?
    done
}

do_upload work/*.pbf &

do_upload work/*.obf &
    
do_upload work/*.poly work/*.geojson &
do_upload work/*-unique.osc.gz &
do_upload work/*-fixmes.osc.gz &
do_upload work/*-addresses.osc.gz &
do_upload work/*.gpx &
do_upload work/*.osn.gz &
do_upload work/boston_*.geojson.gz &

wait
