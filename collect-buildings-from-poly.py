#!/usr/bin/python3

import sys

import shapely.wkt
import shapely.geometry
from shapely.geometry import (Point, MultiPoint, LineString, Polygon)

from utils import get_connection

AREA_BUFFER = 1 / 111325 * 100

SCHEMA = [
    """
    DROP TABLE IF EXISTS `osm_buildings`
    """,
    """
    CREATE TABLE `osm_buildings` (
        `id` BIGINT(64) NOT NULL PRIMARY KEY,
        `poly` POLYGON NOT NULL,
        `centroid` POINT NOT NULL,
        `neighborhood_id` SMALLINT NULL,
        `has_entrance` BOOLEAN DEFAULT FALSE,
        CONSTRAINT FOREIGN KEY(`neighborhood_id`)
            REFERENCES `neighborhoods`(`id`),
        SPATIAL KEY(`poly`)
    ) ENGINE=Aria
    """
]

BUILDING_INSERT_STMT = """
    INSERT into osm_buildings (
        `id`,
        `poly`,
        `centroid`,
        `neighborhood_id`
    ) VALUES(
        %s,
        PolyFromText(%s),
        PointFromText(%s),
        %s
    )
"""


if __name__ == "__main__":
    polygon_file = sys.argv[1]

    conn = get_connection()

    cursor = conn.cursor()

    for item in SCHEMA:
        cursor.execute(item)

    mpoints = []
    # extremely simple parsing here
    with open(polygon_file, 'r', encoding='ascii') as fh:
        for line in fh:
            if line.startswith(' ') or line.startswith('\t'):
                pos = [ x for x in line.strip().split(' ') if len(x) > 0 ]
                mpoints.append(Point(float(pos[0]), float(pos[1])))

    print("Got {} polygon points".format(len(mpoints)))

    mline = LineString(MultiPoint(mpoints))
    polygon = Polygon(mline)

    print("Reading neighborhoods, adding buffer zone...");

    neighborhoods = {}

    cursor.execute("SELECT `id`, `name`, AsWKT(`poly`) AS `poly` FROM `neighborhoods`")

    for item in cursor:
        poly = shapely.wkt.loads(item['poly'].decode('ascii'))
        # Do not add buffer here - the collection is done based on the outer
        # polygon, not the neighborhood one. This is used to match complete
        # buildings.
        neighborhoods[item['id']] = poly

    print("Composing building polygons...")

    res = cursor.execute("""
        SELECT
            `way_nodes`.`id` AS `way_id`,
            `way_nodes`.`sequence_id` AS `sequence_id`,
            AsWKT(`node_positions`.`pos`) AS `point`
        FROM 
            way_tags
        LEFT JOIN
            `way_nodes` ON `way_nodes`.`id` = `way_tags`.`id`
        INNER JOIN
            `node_positions` ON `node_positions`.`id` = `way_nodes`.`node_id`
        WHERE 
            `k` = 'building' AND
            'v' != 'no' AND
            ST_CONTAINS(PolyFromText(%s), `node_positions`.`pos`)
        ORDER BY `way_id`, `sequence_id`
    """, (polygon.buffer(AREA_BUFFER).wkt, ))

    count = 0
    mline = []
    buildings = []
    way_id = None
    cursor2 = conn.cursor()
    for row_id, item in enumerate(cursor, 1):
        if row_id % 1000 == 0:
            print("\r{}/{} ... ".format(row_id, cursor.rowcount), end='')

        if (way_id != None and way_id != item['way_id']) or row_id == cursor.rowcount:
            # next way_id started or at the end of the data
            if len(mline) < 4:
                # triangle ABC will yield 4 points:
                #   0: start A
                #   1: point B
                #   2: point C
                #   3: reference to A
                print("[ way {} ]".format(way_id), end='')
            else:
                mpoint = MultiPoint(mline)
                poly = Polygon(mpoint)

                if poly.is_empty:
                    print("[ way {} ]".format(way_id), end='')
                else:
                    neighborhood_id = None
                    for n_id, n_poly in neighborhoods.items():
                        if n_poly.contains(poly.centroid):
                            neighborhood_id = n_id
                            break

                    buildings.append((way_id, poly, poly.centroid, neighborhood_id))
                    count += 1

                if len(buildings) > 250:
                    cursor2.executemany(BUILDING_INSERT_STMT, buildings)
                    buildings = []
            mline = []

        way_id = item['way_id']
        if item['point'] is None:
            raise Exception(item)
        mline.append(shapely.wkt.loads(item['point']))

    if len(buildings) > 0:
        cursor2.executemany(BUILDING_INSERT_STMT, buildings)

    print("Updating buildings with entrances")

    cursor.execute("""
        UPDATE
            `osm_buildings`
        INNER JOIN `way_nodes`
            ON `osm_buildings`.`id` = `way_nodes`.`id`
        INNER JOIN `node_tags` AS `entrance_tag`
            ON (`entrance_tag`.`id` = `way_nodes`.`node_id` AND
                `entrance_tag`.`k` = 'entrance' AND
                `entrance_tag`.`v` != 'no')
        INNER JOIN `node_tags` AS `housenumber_tag`
            ON (`housenumber_tag`.`id` = `way_nodes`.`node_id` AND
                `housenumber_tag`.`k` = 'addr:housenumber' AND
                `housenumber_tag`.`v` IS NOT NULL)
        SET `has_entrance`=TRUE;
    """)

    conn.commit()

    print("Found {} buildings".format(count))
