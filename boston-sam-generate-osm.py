#!/usr/bin/python3

import fiona
import json
import sys
import MySQLdb
import MySQLdb.cursors
import shapely.geometry
import shapely.wkt
import shapely.wkb

from shapely.geometry import (Point, MultiPoint, Polygon, LineString, mapping)

import xml.etree.ElementTree as ET

from utils import (OSNSerializer, OSCSerializer, GPXSerializer, get_connection)
from polygon2poly import PolySerializer

# addr:city
CITY = 'Boston'

SCHEMA = [
    """
        CREATE TEMPORARY TABLE `results_all` (
            `id` bigint(64),
            `sam_location` POINT NOT NULL,
            `sam_id` BIGINT(64) NOT NULL,
            `sam_housenumber` VARCHAR(256) DEFAULT NULL,
            `osm_housenumber` VARCHAR(256) DEFAULT NULL,
            `sam_street` VARCHAR(256) DEFAULT NULL,
            `osm_street` VARCHAR(256) DEFAULT NULL,
            `sam_postcode` VARCHAR(64) DEFAULT NULL,
            `osm_postcode` VARCHAR(64) DEFAULT NULL,
            `fuzzy` BOOLEAN
        )
    """,
    """
        CREATE TEMPORARY TABLE `results_approx` (
            `id` bigint(64),
            `sam_location` POINT NOT NULL,
            `sam_id` BIGINT(64) NOT NULL,
            `sam_housenumber` VARCHAR(256) DEFAULT NULL,
            `osm_housenumber` VARCHAR(256) DEFAULT NULL,
            `sam_street` VARCHAR(256) DEFAULT NULL,
            `osm_street` VARCHAR(256) DEFAULT NULL,
            `sam_postcode` VARCHAR(64) DEFAULT NULL,
            `osm_postcode` VARCHAR(64) DEFAULT NULL,
            `fuzzy` BOOLEAN
        )
    """,
    """
        CREATE TEMPORARY TABLE `results_combined` (
            `id` bigint(64),
            `sam_location` POINT NOT NULL,
            `sam_id` BIGINT(64) NOT NULL UNIQUE,
            `sam_housenumber` VARCHAR(256) DEFAULT NULL,
            `osm_housenumber` VARCHAR(256) DEFAULT NULL,
            `sam_street` VARCHAR(256) DEFAULT NULL,
            `osm_street` VARCHAR(256) DEFAULT NULL,
            `sam_postcode` VARCHAR(64) DEFAULT NULL,
            `osm_postcode` VARCHAR(64) DEFAULT NULL,
            `fuzzy` BOOLEAN
        )
    """
]

if __name__ == "__main__":

    neighborhood = sys.argv[1]

    filename = neighborhood.replace('/', '_')

    conn = get_connection()

    cursor = conn.cursor();

    for item in SCHEMA:
        res = cursor.execute(item)

    print("Looking for buildings spanning multiple tax parcels...")

    res = cursor.execute("""
        SELECT
            `osm_buildings`.`id` AS `id`,
            count(*) AS `c`
        FROM
            `osm_buildings`
        INNER JOIN `neighborhoods`
            ON ST_CONTAINS(`neighborhoods`.`poly`, `osm_buildings`.`centroid`)
        LEFT JOIN `tax_parcels`
            ON ST_INTERSECTS(`tax_parcels`.`poly`, `osm_buildings`.`poly`)
        WHERE
            `neighborhoods`.`name` = %s AND
            ST_CONTAINS(`neighborhoods`.`poly`, `tax_parcels`.`centroid`)
        GROUP BY `id`
        HAVING `c` > 1
    """, (neighborhood, ))

    undersplit_buildings = set()

    for item in cursor:
        undersplit_buildings.add(item['id'])

    print("Found {} undersplit buildings".format(len(undersplit_buildings)))

    print("Searching for address nodes...")
    res = cursor.execute("""
        SELECT
            `node_tags`.`id` AS `id`,
            AsWKT(`node_positions`.`pos`) AS `pos`,
            `v`
        FROM
            `node_tags`
        INNER JOIN `node_positions`
            ON `node_tags`.`id` = `node_positions`.`id`
        INNER JOIN `neighborhoods`
            ON `neighborhoods`.`name` = %s AND
            ST_CONTAINS(`neighborhoods`.`poly`, `node_positions`.`pos`)
        WHERE
            `k` = 'addr:housenumber'
    """, (neighborhood, ))

    existing_address_nodes = {}

    for item in cursor:
        existing_address_nodes[
                shapely.wkt.loads(item['pos'].decode('ascii'))
        ] = item['id']

    print("Found {} address nodes".format(len(existing_address_nodes)))

    print("Building results mapping")

    res = cursor.execute("""
        INSERT INTO `results_all`
        SELECT
            `osm_buildings`.`id`,
            `SAM_Addresses`.`LOCATION` AS `sam_location`,
            `SAM_Addresses`.`SAM_ADDRESS_ID` AS `sam_id`,
            TRIM(`SAM_Addresses`.`STREET_NUMBER`) AS `sam_housenumber`,
            `housenumber_tag`.`v` AS `osm_housenumber`,
            `sam_streets`.`name` AS `sam_street`,
            `osm_streets`.`name` AS `osm_street`,
            `SAM_Addresses`.`ZIP_CODE` AS `sam_postcode`,
            `postcode_tag`.`v` AS `osm_postcode`,
            `ossm`.`fuzzy` AS `fuzzy`
        FROM
            `SAM_Addresses`
        INNER JOIN `neighborhoods`
            ON ST_CONTAINS(`neighborhoods`.`poly`, `SAM_Addresses`.`LOCATION`)
        INNER JOIN `osm_sam_street_mappings` `ossm`
            ON `SAM_Addresses`.`STREET_ID` = `ossm`.`sam_street_id`
        INNER JOIN `sam_streets`
            ON `sam_streets`.`id` = `ossm`.`sam_street_id`
        LEFT JOIN `osm_buildings`
            ON (`osm_buildings`.`neighborhood_id` = `neighborhoods`.`id` AND
                ST_CONTAINS(`osm_buildings`.`poly`, `SAM_Addresses`.`LOCATION`))
        LEFT JOIN `way_tags` AS `building_tag`
            ON (`osm_buildings`.`id` = `building_tag`.`id` AND `building_tag`.`k` = 'building')
        LEFT JOIN `osm_streets`
            ON `osm_streets`.`id` = `ossm`.`osm_street_id`
        LEFT JOIN `way_tags` AS `housenumber_tag`
            ON (`building_tag`.`id` = `housenumber_tag`.`id` AND `housenumber_tag`.`k` = 'addr:housenumber')
        LEFT JOIN `way_tags` AS `postcode_tag`
            ON (`building_tag`.`id` = `postcode_tag`.`id` AND `postcode_tag`.`k` = 'addr:postcode')
        LEFT JOIN `way_tags` AS `source_addr_tag`
            ON (`building_tag`.`id` = `source_addr_tag`.`id` AND `source_addr_tag`.`k` = 'source:addr')
        WHERE
            `SAM_Addresses`.`STREET_NUMBER` IS NOT NULL
            -- restrict to neighborhood
            AND `neighborhoods`.`name` = %s
            -- don't include things that were manually surveyed
            AND (`source_addr_tag`.`v` IS NULL OR `source_addr_tag`.`v` != 'survey')
            -- ignore buildings with defined entrances
            AND (`osm_buildings`.`has_entrance` IS NULL OR
                 `osm_buildings`.`has_entrance` != TRUE)
    """, (neighborhood, ))

    print("Searching for buildings nearby...")

    # Cannot reopen temporary table
    res = cursor.execute("""
        INSERT INTO `results_approx`
        SELECT
            `osm_buildings`.`id`,
            `sam_location`,
            `sam_id`,
            `sam_housenumber`,
            `housenumber_tag`.`v` AS `osm_housenumber`,
            `sam_street`,
            `osm_streets`.`name` AS `osm_street`,
            `sam_postcode`,
            `postcode_tag`.`v` AS `osm_postcode`,
            `ossm`.`fuzzy` AS `fuzzy`
        FROM `results_all`
        INNER JOIN `osm_buildings`
            ON ST_DISTANCE(`osm_buildings`.`poly`, `sam_location`) < (1 / 111325 * 5)
        INNER JOIN `tax_parcels`
            ON (ST_CONTAINS(`tax_parcels`.`poly`, `sam_location`) AND
                ST_CONTAINS(`tax_parcels`.`poly`, `osm_buildings`.`centroid`))
        INNER JOIN `SAM_Addresses`
            ON `sam_id` = `SAM_Addresses`.`SAM_ADDRESS_ID`
        INNER JOIN `osm_sam_street_mappings` `ossm`
            ON `SAM_Addresses`.`STREET_ID` = `ossm`.`sam_street_id`
        LEFT JOIN `way_tags` AS `building_tag`
            ON (`osm_buildings`.`id` = `building_tag`.`id` AND `building_tag`.`k` = 'building')
        LEFT JOIN `osm_streets`
            ON `osm_streets`.`id` = `ossm`.`osm_street_id`
        LEFT JOIN `way_tags` AS `housenumber_tag`
            ON (`building_tag`.`id` = `housenumber_tag`.`id` AND `housenumber_tag`.`k` = 'addr:housenumber')
        LEFT JOIN `way_tags` AS `postcode_tag`
            ON (`building_tag`.`id` = `postcode_tag`.`id` AND `postcode_tag`.`k` = 'addr:postcode')
        WHERE `results_all`.`id` IS NULL
    """)

    print("Combining results...")

    res = cursor.execute("""
        INSERT IGNORE INTO `results_combined`
        SELECT
            `id`,
            `sam_location`,
            `sam_id`,
            `sam_housenumber`,
            `osm_housenumber`,
            `sam_street`,
            `osm_street`,
            `sam_postcode`,
            `osm_postcode`,
            `fuzzy`
        FROM
            `results_approx`
        UNION
        SELECT
            `id`,
            `sam_location`,
            `sam_id`,
            `sam_housenumber`,
            `osm_housenumber`,
            `sam_street`,
            `osm_street`,
            `sam_postcode`,
            `osm_postcode`,
            `fuzzy`
        FROM
            `results_all`
        WHERE
            `osm_housenumber` != `sam_housenumber`
            OR `osm_postcode` != `sam_postcode`
            OR `osm_street` IS NULL
            OR `osm_housenumber` IS NULL
            OR `osm_postcode` IS NULL
    """)

    print("Processing results")

    res = cursor.execute("""
        SELECT
            `results_combined`.`id` AS `id`,
            AsWKT(`sam_location`) AS `sam_location`,
            `sam_id`,
            `sam_housenumber`,
            `osm_housenumber`,
            `sam_street`,
            `osm_street`,
            `sam_postcode`,
            `osm_postcode`,
            `fuzzy`,
            AsWKT(`osm_buildings`.`poly`) AS `poly`
        FROM
            `results_combined`
        LEFT JOIN `osm_buildings`
            ON `results_combined`.`id` = `osm_buildings`.`id`
    """)

    ways = {}
    notes = []

    for row in cursor:
        way_id = row['id']
        sam_location = row['sam_location']
        sam_housenumber = row['sam_housenumber']
        osm_housenumber = row['osm_housenumber']
        sam_street = row['sam_street']
        osm_street = row['osm_street']
        sam_postcode = row['sam_postcode']
        osm_postcode = row['osm_postcode']
        fuzzy = row['fuzzy']
        poly = row['poly']
        if poly is not None:
            poly = shapely.wkt.loads(poly.decode('ascii'))

        location = shapely.wkt.loads(sam_location.decode('ascii'))

        if way_id is None or osm_street is None or fuzzy:
            # Missing building notes will appear after street notes,
            # since you need to have a street to put a building on it
            if way_id is None:
                notes.append({
                    'location': location,
                    'text': "Missing: {} {}".format(
                        sam_housenumber, sam_street),
                    'comment': "Building is present in SAM but missing in OSM",
                    'kind': 'missing object'
                })

            # osm_street can be:
            # present/not fuzzy if it is 1-1 correspondence
            # present/fuzzy if it is just looks like SAM one
            # not present
            if osm_street is None:
                notes.insert(0, {
                    'location': location,
                    'text': 'Unknown street ({} {})'.format(
                        sam_housenumber, sam_street),
                    'comment': "OSM has no match for SAM street",
                    'kind': 'unknown street'
                })

            if fuzzy:
                notes.insert(0, {
                    'location': location,
                    'text': 'Fuzzy: OSM: {}; SAM: {}'.format(
                        osm_street, sam_street
                    ),
                    'comment': "Fuzzy street match",
                    'kind': 'fuzzy street match'
                })

            # if notes added, don't add building
            continue

        if way_id not in ways:
            ways[way_id] = {
                'version': -1,
                'nd': [],
                'tags': {},
                '_poly': poly,
                '_markers': []
            }

        way = ways[way_id]

        # info from db, so duplicate info is a fixme
        fixme = []
        if 'fixme' in way['tags']:
            fixme = [ f.strip() for f in way['tags']['fixme'].split(';') ]

        existing_housenumber = way['tags'].get('addr:housenumber', None)
        existing_street = way['tags'].get('addr:street', None)
        existing_postcode = way['tags'].get('addr:postcode', None)

        if existing_street is not None and existing_street != osm_street:
            fixme.append("+addr:street={}".format(existing_street))

        if existing_housenumber is not None and (existing_housenumber != sam_housenumber):
            # prefer ranges to get better coverage
            housenumber_fixme = None

            if existing_street != osm_street:
                # street mismatch, will be handled above
                pass
            elif '-' in sam_housenumber and '-' in existing_housenumber:
                # too many ranges...
                housenumber_fixme = "+addr:housenumber={} (multiple ranges)"
            else:
                try:
                    if '-' in sam_housenumber:
                        range_arg = [ int(x) for x in sam_housenumber.split('-') ]
                        r = range(range_arg[0], range_arg[1]+1)
                        if int(existing_housenumber) not in r:
                            housenumber_fixme = "+addr:housenumber={}"
                        # else sam_housenumber will override the address
                    elif '-' in existing_housenumber:
                        range_arg = [ int(x) for x in existing_housenumber.split('-') ]
                        r = range(range_arg[0], range_arg[1]+1)
                        if int(sam_housenumber) in r:
                            # override with range
                            sam_housenumber = existing_housenumber
                except ValueError:
                    print("Skipping range new={}, existing={}".format(
                            sam_housenumber, existing_housenumber))
                    # can't figure the range, leave as fixme
                    housenumber_fixme = "+addr:housenumber={}"

            if housenumber_fixme is not None:
                fixme.append(housenumber_fixme.format(sam_housenumber))

        # if we already have the address in OSM and
        # it differs by more than punctuation
        if osm_housenumber is not None and osm_housenumber != sam_housenumber:
            root_osm_housenumber = osm_housenumber.replace(' ', '').replace('-', '')
            root_sam_househumber = sam_housenumber.replace(' ', '').replace('-', '')

            if root_osm_housenumber != root_sam_househumber:
                fixme.append("+addr:housenumber={} (osm)".format(osm_housenumber))

        if osm_postcode is None:
            way['tags']['addr:postcode'] = sam_postcode
        elif not osm_postcode.startswith(sam_postcode):
            # support ZIP+4, if set by somebody
            way['tags']['addr:postcode'] = sam_postcode
            fixme.append("+addr:postcode={}".format(osm_postcode))

        # last one wins, kind of
        way['tags']['addr:housenumber'] = sam_housenumber
        way['tags']['addr:street'] = osm_street
        way['tags']['addr:city'] = CITY
        if len(fixme) > 0:
            way['tags']['fixme'] = '; '.join(fixme)

        # save marker for notes/gpx
        way['_markers'].append({
            'location': location,
            'addr:housenumber': sam_housenumber,
            'addr:street': osm_street,
            'addr:postcode': sam_postcode,
            'addr:city':  CITY
        })

        way['_undersplit'] = True if way_id in undersplit_buildings else False

    print("Reconstructing ways XML: tags")

    cursor.execute("""
        SELECT
            `ways`.`id` AS `way_id`,
            `ways`.`version` AS `version`,
            `way_tags`.`k` AS `k`,
            `way_tags`.`v` AS `v`
        FROM
            `ways`
        LEFT JOIN `way_tags`
            ON `way_tags`.`id` = `ways`.`id`
        WHERE `ways`.`id` IN (
            SELECT `id` FROM `results_combined`
            WHERE `id` IS NOT NULL
                AND `osm_street` IS NOT NULL
                AND `fuzzy` = FALSE
        )
    """)

    for row in cursor:
        way = ways[row['way_id']]
        way['version'] = row['version']

        if row['k'] not in way['tags']:
            # don't overwrite existing information
            way['tags'][row['k']] = row['v']

    print("Reconstructing ways XML: nodes")

    cursor.execute("""
        SELECT
            `way_nodes`.`id` AS `way_id`,
            `way_nodes`.`sequence_id` AS `sequence_id`,
            `way_nodes`.`node_id` AS `node_id`
        FROM `way_nodes`
        WHERE `way_nodes`.`id` IN (
            SELECT `id` FROM `results_combined`
            WHERE `id` IS NOT NULL
                AND `osm_street` IS NOT NULL
                AND `fuzzy` = FALSE
        )
        ORDER BY `way_nodes`.`id`, `way_nodes`.`sequence_id`
    """)

    for row in cursor:
        way = ways[row['way_id']]
        way['nd'].append(row['node_id'])
        assert way['nd'][row['sequence_id']] == row['node_id']

    conn.close()

    unique_ways = {
        k: v for k,v in ways.items()
            if '+addr:' not in v['tags'].get('fixme', '')
    }

    address_nodes = []

    fixme_ways = {
        k: v for k,v in ways.items()
            if '+addr:' in v['tags'].get('fixme', '')
    }

    for way_id, data in fixme_ways.items():
        for marker in data['_markers']:
            prepend = False
            if '(osm)' in data['tags']['fixme']:
                text = 'Wrong addr: {} {}?'
                kind = 'wrong address'
            elif data['_undersplit']:
                text = 'Split: {} {}'
                kind = 'split building'
                prepend = True
            else:
                # Only add address node
                if marker['location'] not in existing_address_nodes:
                    address_nodes.append({
                        'location': marker['location'],
                        'tags': {
                            'addr:street': marker['addr:street'],
                            'addr:housenumber': marker['addr:housenumber'],
                            'addr:postcode': marker['addr:postcode'],
                            'addr:city': marker['addr:city']
                        }
                    })
                else:
                    print("Not adding address node")
                continue

            note = {
                'location': marker['location'],
                'text': text.format(
                    marker['addr:housenumber'],
                    marker['addr:street']
                ),
                'comment': data['tags']['fixme'],
                'kind': kind
            }

            if prepend:
                notes.insert(0, note)
            else:
                notes.append(note)

    ocs = OSCSerializer()
    building_schema = {
        'geometry': 'Polygon',
        'properties': {
            'addr:housenumber': 'str:64',
            'addr:street': 'str:128',
            'addr:postcode': 'str:16',
            'addr:city': 'str:6'
        }
    }

    fixme_schema = {
        'geometry': 'Point',
        'properties': {
            'kind': 'str:16',
            'text': 'str:128',
            'comment': 'str:128'
        }
    }

    print("Writing {}-addresses.osc".format(filename))
    print("\t{} new address nodes".format(len(address_nodes)))
    with open("{}-addresses.osc".format(filename), 'wb') as changefile:
        changefile.write(ocs.serialize(None, address_nodes))

    print("Writing {}-unique.osc".format(filename))
    with open("{}-unique.osc".format(filename), 'wb') as changefile:
        changefile.write(ocs.serialize(unique_ways))

    print("Writing {}-unique.geojson".format(filename))
    with fiona.open("{}-unique.geojson".format(filename), 'w',
            'GeoJSON', building_schema) as geojson:
        for way_id, way_data in unique_ways.items():
            geojson.write({
                'geometry': mapping(way_data['_poly']),
                'properties': {
                    k: v for k, v in way_data['tags'].items()
                        if k in building_schema['properties'] }
            })

    print("Writing {}-fixmes.osc".format(filename))
    with open("{}-fixmes.osc".format(filename), 'wb') as changefile:
        changefile.write(ocs.serialize(fixme_ways))

    if len(notes) > 0:
        print("Writing notes:\n\t{}.osn".format(filename))
        notedump = OSNSerializer()
        with open('{}.osn'.format(filename), 'wb') as fh:
            xml = notedump.serialize(notes)
            fh.write(xml)

        print("\t{}.gpx".format(filename))
        gpxdump = GPXSerializer()
        with open('{}.gpx'.format(filename), 'wb') as fh:
            xml = gpxdump.serialize(notes)
            fh.write(xml)

        print("\t{}-fixmes.geojson".format(filename))
        with fiona.open('{}-fixmes.geojson'.format(filename), 'w',
                'GeoJSON', fixme_schema) as geojson:
            for item in notes:
                geojson.write({
                    'geometry': mapping(item['location']),
                    'properties': {
                        'kind': item['kind'],
                        'text': item['text'],
                        'comment': item.get('comment', '')
                    }
                })
    else:
        print("Not writing notes file")

    stats = {
        'n': len(notes),
        'u': len(unique_ways),
        'f': len(fixme_ways),
        'a': len(address_nodes)
    }

    print("Stats:")
    with open("{}-stats.json".format(filename), 'w') as statfile:
        statfile.write(json.dumps(stats))
        print("\t{} Unique: {}".format(neighborhood, stats['u']))
        print("\t{} Fixmes: {}".format(neighborhood, stats['f']))
        print("\t{} Notes: {}".format(neighborhood, stats['n']))
