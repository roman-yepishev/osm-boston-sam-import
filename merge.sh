#!/bin/sh

osmconvert massachusetts-latest.osm.pbf \
    | osmchange64 work/*-unique.osc \
    | osmconvert - --out-pbf -o=work/massachusetts-latest+boston-sam.osm.pbf

