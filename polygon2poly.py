#!/usr/bin/python3

import sys
from shapely.geometry import (MultiPolygon)
import shapely.wkt

class PolySerializer(object):

    def serialize(self, poly, name=None):
        if name is None:
            name = 'polygon'

        if not isinstance(poly, MultiPolygon):
            poly = MultiPolygon([poly])

        result = [name]

        geom_id = 0

        for idx, geom in enumerate(poly):
            geom_id += 1

            result.append(geom_id)

            for point in geom.exterior.coords:
                result.append("    {:E} {:E}".format(point[0], point[1]))

            result.append('END')

            for hole in geom.interiors:
                geom_id += 1
                result.append("!{}".format(geom_id))

                for point in hole.coords:
                    result.append("    {:E} {:E}".format(point[0], point[1]))

                result.append('END')

        result.append('END')

        return "\n".join([ str(x) for x in result ])

if __name__ == "__main__":
    with open(sys.argv[1], 'r', encoding='ascii') as fh:
        poly = shapely.wkt.loads(fh.read())
        serializer = PolySerializer()
        print(serializer.serialize(poly))
