Boston SAM Import (spaghetti code)
==================================

This is repository for [Boston SAM Import](https://wiki.openstreetmap.org/wiki/Import/Boston_Street_Address_Management_%28SAM%29_Import).

This branch contains a working version that you probably don't want to look at (but feel free to learn a few tricks).

Prerequisites
=============

* python3
* MariaDB server
* osmconvert
* Python3 modules:
    * MySQLdb
    * fiona (you may have to compile your own libgdal.so and play with `LD_LIBRARY_PATH` if your distro ships v2.x)
    * shapely

Usage
=====

See `do-all-things.sh` and `batch.sh` scripts for the semi-automatic way I'm using.

Download:

* `massachusetts-latest.osm.pbf` from [Geofabrik](http://download.geofabrik.de/north-america/us/massachusetts.html)
* `parcels_14.zip` from [BostonMaps](http://boston.maps.arcgis.com/home/item.html?id=cd6d9058ee9d4475b924751a2bb9d263)
* SAM GeoJSON from [Boston Open Data](http://bostonopendata.boston.opendata.arcgis.com/datasets/b6bffcace320448d96bb84eabb8a075f_0)


Use `osmconvert` to extract Boston:
```
  osmconvert -v massachusetts-latest.osm.pbf \
    -B=boston_massachusetts.poly \
    -o=boston_massachusetts_area.osm
```

Import OSM Data:
```
python3 ./osm2mariadb.py boston_massachusetts_area.osm
```

Import Tax Parcels:
```
./collect-tax-parcels.py parcels_14/parcels_14.shp
```

Collect neghborhoods:
```
python3 ./collect-neighborhoods.py
```

Collect buildings:
```
python3 ./collect-buildings-from-poly.py boston_massachusetts.poly
```

Import SAM GeoJSON:
```
python3 ./samjson2mariadb.py b6bffcace320448d96bb84eabb8a075f_0.geojson
```

Create Street Mapping
```
mysql -uosm osm < street-map.sql
```

Generate .osc, .osn, .gpx, -stats.json files:
```
python3 ./boston-sam-generate-osm.py 'Neighborhood Name'
```

Stop. Hammertime!

Don't upload anything to OSM unless you are me.

License
=======

This work has been identified as being free of known restrictions under
copyright law, including all related and neighboring rights.

You can copy, modify, distribute and perform the work, even for commercial
purposes, all without asking permission.

* The work may not be free of known copyright restrictions in all
jurisdictions.
* Persons may have other rights in or related to the work, such as patent or
trademark rights, and others may have rights in how the work is used, such as
publicity or privacy rights.
* In some jurisdictions moral rights of the author may persist beyond the
term of copyright. These rights may include the right to be identified as the
author and the right to object to derogatory treatments.
* Unless expressly stated otherwise, the person who identified the work
makes no warranties about the work, and disclaims liability for all uses of
the work, to the fullest extent permitted by applicable law.
* When using or citing the work, you should not imply endorsement by the
author or the person who identified the work.

See https://creativecommons.org/publicdomain/mark/1.0/
