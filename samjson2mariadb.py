#!/usr/bin/python3

import sys
import json

from utils import get_connection

MAIN_TABLE_SCHEMA = """
CREATE TABLE IF NOT EXISTS SAM_Addresses
(
    `SAM_ADDRESS_ID` INTEGER PRIMARY KEY,
    `LOCATION` POINT NOT NULL,
    `STREET_NUMBER` VARCHAR(255) NULL,
    `FULL_STREET_NAME` VARCHAR(255),
    `STREET_PREFIX` VARCHAR(255) NULL,
    `STREET_BODY` VARCHAR(255),
    `STREET_FULL_SUFFIX` VARCHAR(64),
    `STREET_ID` BIGINT(64),
    `ZIP_CODE` VARCHAR(64),
    SPATIAL KEY(`LOCATION`)
) ENGINE=Aria DEFAULT CHARSET=utf8
"""

if __name__ == "__main__":
    conn = get_connection()

    c = conn.cursor()
    c.execute('DROP TABLE IF EXISTS SAM_Addresses')
    c.execute(MAIN_TABLE_SCHEMA)

    with open(sys.argv[1], 'r', encoding='utf-8') as fh:
        data = json.loads(fh.read())

        pa_insert_stmt = """
            INSERT INTO SAM_Addresses (
                `SAM_ADDRESS_ID`,
                `LOCATION`,
                `STREET_NUMBER`,
                `FULL_STREET_NAME`,
                `STREET_PREFIX`,
                `STREET_BODY`,
                `STREET_FULL_SUFFIX`,
                `STREET_ID`,
                `ZIP_CODE`
            ) VALUES (
                %(SAM_ADDRESS_ID)s,
                POINT(%(X)s,%(Y)s),
                %(STREET_NUMBER)s,
                %(FULL_STREET_NAME)s,
                %(STREET_PREFIX)s,
                %(STREET_BODY)s,
                %(STREET_FULL_SUFFIX)s,
                %(STREET_ID)s,
                %(ZIP_CODE)s
            )
        """

        cache = []
        for feature in data["features"]:
            props = feature["properties"]
            if props['RELATIONSHIP_TYPE'] != 1:
                continue

            row = props.copy()

            for prop_key, prop_value in row.items():
                if isinstance(prop_value, str):
                    prop_value = prop_value.strip()
                    if prop_value == '':
                        row[prop_key] = None

            row['X'] = feature['geometry']['coordinates'][0]
            row['Y'] = feature['geometry']['coordinates'][1]

            cache.append(row)

            if len(cache) > 500:
                c.executemany(pa_insert_stmt, cache)
                cache = []

        if len(cache) > 0:
            c.executemany(pa_insert_stmt, cache)

    conn.commit();
    conn.close();
