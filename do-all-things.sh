#!/bin/sh

set -e -x

export OSM_DB_HOST=localhost
export OSM_DB_NAME=osm
export OSM_DB_PASSWORD=3820f01f4bc4
export OSM_DB_USER=rye

DB_HOSTS="localhost perigee.local"

if [ massachusetts-latest.osm.pbf -nt boston_massachusetts_area.osm ]; then
    osmconvert -v massachusetts-latest.osm.pbf \
        -B=boston_massachusetts.poly \
        -o=boston_massachusetts_area.osm
fi

function do_process() {
    #./collect-tax-parcels.py parcels_14/parcels_14.shp
    ./osm2mariadb.py boston_massachusetts_area.osm
    ./collect-neighborhoods.py
    ./collect-buildings-from-poly.py boston_massachusetts.poly

    mysql -u$OSM_DB_USER -p$OSM_DB_PASSWORD -h$OSM_DB_HOST $OSM_DB_NAME  < street-map.sql
}

for H in $DB_HOSTS; do
    export OSM_DB_HOST=$H
    do_process &
done

wait

./batch.sh
